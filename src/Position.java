
public class Position {
	
	private int x;
	private int y;
	
/*
 * This will be the position of any piece of the snake. 
 */
	public Position(int x, int y) {
		this.setX(x);
		this.setY(y);
	}
/*
 * These methods will allow the x and y coordinates to be isolated.
 */
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

}
