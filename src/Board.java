
public class Board {
//Numbers are same but different names helps readability.
	private final int BOARD_HEIGHT = 50;
	private final int BOARD_WIDTH = 50;
	
	private char[][] board = new char[BOARD_HEIGHT][BOARD_WIDTH];
	
	public Board() {
				
	}

/*
 * The orientation of the x and y inside the board will depend
 * on how we want to grid the board.	
 */
	public void setChar(char piece, int x, int y) {
		board[x][y] = piece;
	}

}
